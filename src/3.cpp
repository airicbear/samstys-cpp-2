#include <iostream>

int main(int argc, char* argv[]) {
  const int dividend = 6;
  const int divisor = 2;
  int result = dividend / divisor;
  std::cout << result << std::endl;
  result += 3;
  std::cout << result << std::endl;
  return 0;
}